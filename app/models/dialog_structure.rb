class DialogStructure < ActiveRecord::Base
  has_one :group
  has_many :dialogs

  belongs_to :parent, class_name: 'DialogStructure'
  has_many :children, foreign_key: :parent_id, class_name: 'DialogStructure'

  attr_accessor :group_id

  before_save :upper_keys
  after_commit :assign_to_group

  def base_element
    base = self
    until base.parent.nil?
      base = base.parent
    end
    base
  end

  protected

  def assign_to_group
    unless group_id.empty?
      group = Group.find group_id.to_i
      group.dialog_structure = self
      group.save
    end
  end

  def upper_keys
    self.key = self.key.mb_chars.upcase.to_s if self.key
    self.back_key = self.back_key.mb_chars.upcase.to_s if self.back_key
    self.reset_key = self.reset_key.mb_chars.upcase.to_s if self.reset_key
  end

end