class Attachment < ActiveRecord::Base
  belongs_to :subject, polymorphic: true
  serialize :full_data, Hash

end