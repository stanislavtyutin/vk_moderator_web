class Bot < ActiveRecord::Base
  has_and_belongs_to_many :groups

  after_commit :sync

  def load_groups
    CoreApi.instance.bot_load_groups self.id
  end

  protected

  def sync
    CoreApi.instance.bot_sync self.id
  end

end