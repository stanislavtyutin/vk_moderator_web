class PromoCodeCampaign < ActiveRecord::Base
  has_many :promo_codes

  before_save :format_keys

  def add_codes(list)
    list.each do |t|
      PromoCode.create promo_code_campaign: self, code: t
    end
  end

  protected

  def format_keys
    if self.key
      self.key = self.key.mb_chars.upcase.to_s.split(',').map { |t| t.strip }.join(', ')
    end
  end

end