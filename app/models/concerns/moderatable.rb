module Moderatable

  def short_text
    short = text[0..99]
    if text.size > short.size
      short + '...'
    else
      short
    end
  end

  def get_removing_reason
    data = CoreApi.instance.get_removing_reason self.class.to_s.downcase, self.id
    result = []
    result << ['В белом листе', data[:in_white_list]] unless data[:in_white_list].nil?
    result << ['Имеется текст', data[:text_exists]] unless data[:text_exists].nil?
    result << ['Таймаут', data[:timeout]] unless data[:timeout].nil?
    result << ['Отсутствуют стоп-слова', data[:stop_words]] unless data[:stop_words].nil?
    result << ['Стоп слова', data[:first_stop_word]] unless data[:first_stop_word].nil?
    result << ['Отсутствуют сслыки', data[:links]] unless data[:links].nil?
    result << ['Состоит в группе', data[:membership]] unless data[:membership].nil?
    result
  end

end