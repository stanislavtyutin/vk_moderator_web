class Comment < ActiveRecord::Base
  include Moderatable

  belongs_to :subject, polymorphic: true
  has_many :attachments, as: :subject

  def group_vk_id
    self.subject.group_vk_id
  end

  def group
    self.subject.group
  end

end