class Group < ActiveRecord::Base
  has_many :posts
  has_many :topics
  has_many :white_links
  has_many :white_users
  has_many :timetable_items
  has_many :promo_codes
  has_and_belongs_to_many :bots
  belongs_to :dialog_structure

  before_save :format_stop_words

  def sync
    CoreApi.instance.group_sync self.id
  end

  protected

  def format_stop_words
    self.stop_words = self.stop_words.split(',').map { |t| t.strip }.join(', ')
  end

end