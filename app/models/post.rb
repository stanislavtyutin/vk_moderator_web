class Post < ActiveRecord::Base
  include Moderatable

  belongs_to :group
  has_many :attachments, as: :subject
  has_many :comments, as: :subject
  has_one :timetable_item

  alias vk_comments comments

  def post_type
    case super
      when 'post'
        'Обычная запись'
      when 'suggest'
        'Предложенная запись'
      when 'copy'
        'Репост'
      when 'reply'
        'Ответ'
      when 'postpone'
        'Отложенная запись'
      else
        'Другое'
    end
  end

end