class TimetableItem < ActiveRecord::Base
  belongs_to :group
  belongs_to :post

  def free?
    !self.reserved && (!self.publish_date || (Time.now > self.publish_date))
  end

  def get_publish_time
    now = Time.now

    time = Time.new now.year, now.month, now.day, self.hour, self.min

    time = Time.new(now.year, now.month, now.day + 1, self.hour, self.min) if time < Time.now

    time
  end

end