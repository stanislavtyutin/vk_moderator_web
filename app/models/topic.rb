class Topic < ActiveRecord::Base
  belongs_to :group
  has_many :comments, as: :subject

  alias vk_comments comments

  def group_vk_id
    self.group.vk_id
  end

end