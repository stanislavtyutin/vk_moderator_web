class WhiteUser < ActiveRecord::Base
  belongs_to :group

  after_commit :sync

  protected

  def sync
    CoreApi.instance.white_user_sync self.id
  end

end