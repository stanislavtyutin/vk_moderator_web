class QuickController < ApplicationController

  def index
    @groups = Group.where(active: true).all
    group_id = params[:group_id]
    if group_id
      @group = Group.find_by_id group_id
      if @group
        @items = @group.posts.where('publish_date > ?', Time.now - 18.hours).order(publish_date: :desc).all.to_a
        @items.clone.each do |item|
          @items += item.comments.all.to_a
        end
        @items.delete_if { |t| t.check_result }
      end
    end
  end

  def reason
    item_id = params.require :id
    item_type = params.require :type

    if item_type == 'Post'
      item = Post.find_by_id item_id
    elsif item_type == 'Comment'
      item = Comment.find_by_id item_id
    else
      raise
    end

    render json: item.get_removing_reason
  rescue
    render json: []
  end

end
