ActiveAdmin.register WhiteUser do
  belongs_to :group
  permit_params :group_id, :name, :vk_id, :start_date, :stop_date

  index do
    selectable_column
    # id_column
    column :name
    column :vk_id do |t|
      link_to t.vk_id, "https://vk.com/id#{t.vk_id}"
    end
    column :start_date
    column :stop_date
    actions
  end

  form do |f|
    f.inputs do
      f.input :group, as: :hidden
      f.input :name
      f.input :vk_id
      f.input :start_date
      f.input :stop_date
    end
    f.actions
  end

end
