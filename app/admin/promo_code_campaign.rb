ActiveAdmin.register PromoCodeCampaign do
  permit_params :name, :key, :active, :template, :min_friends_count, :min_friends_fail_message

  menu parent: 'Дополнительные функции', priority: 9

  index do
    selectable_column
    column :name
    column :key
    column :keys_count do |t|
      t.promo_codes.count
    end
    column :remained_keys_count do |t|
      t.promo_codes.where(user_vk_id: nil).count
    end
    column :active
    actions
  end

  show do
    attributes_table do
      row :name
      row :key
      row :active
      row :min_friends_count
      row :keys_count do
        promo_code_campaign.promo_codes.count
      end
      row :remained_keys_count do
        promo_code_campaign.promo_codes.where(user_vk_id: nil).count
      end
    end

    panel 'Последние розданные коды' do
      table_for promo_code_campaign.promo_codes.where.not(user_vk_id: nil).order(updated_at: :desc).take(10) do
        column('Промокод') { |t| t.code }
        column('Группа') { |t| t.group&.name }
        column('ID пользователя') { |t| t.user_vk_id }
        column('Дата и время') { |t| t.updated_at.strftime('%Y-%m-%d %H:%M:%S') }
      end
    end

    panel 'Статистика по группам' do
      data = Group.all.map do |group|
        if group.promo_codes.where(promo_code_campaign: promo_code_campaign).any?
          {name: group.name, count: group.promo_codes.where(promo_code_campaign: promo_code_campaign).count}
        else
          nil
        end
      end
      data = data.compact

      table_for data do
        column('Группа') { |t| t[:name] }
        column('Количество') { |t| t[:count] }
      end
    end

    panel 'Добавить коды' do
      render partial: 'add_codes', locals: {promo_code_capmaign: promo_code_campaign}
    end

    active_admin_comments
  end

  form do |f|
    f.semantic_errors
    f.inputs :name, :key, :active, :template, :min_friends_count, :min_friends_fail_message
    f.actions
  end

  filter :name
  filter :key
  filter :active

  member_action :add_codes, method: :post do
    obj = PromoCodeCampaign.find params[:id]
    obj.add_codes params[:codes].lines.map { |t| t.strip }.delete_if { |t| t.empty? }
    return redirect_to admin_promo_code_campaign_path(obj), notice: 'Промо-коды добавлены'
  end

end