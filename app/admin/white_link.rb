ActiveAdmin.register WhiteLink do
  belongs_to :group
  permit_params :group_id, :link, :start_date, :stop_date

  index do
    selectable_column
    # id_column
    column :link
    column :start_date
    column :stop_date
    actions
  end

  form do |f|
    f.inputs do
      f.input :group, as: :hidden
      f.input :link
      f.input :start_date
      f.input :stop_date
    end
    f.actions
  end

end
