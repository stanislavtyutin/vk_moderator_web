ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: 'Обзор'

  content title: 'Обзор' do

    columns do
      column do
        panel 'Последние посты' do
          table_for Post.order(id: :desc).limit(10) do

            column('Проверка') do |post|
              if post.check_result
                status_tag 'ok', :ok
              else
                status_tag 'fail', :red
              end
            end

            column('Группа') { |t| link_to t.group.name, admin_group_path(t.group) }

            column('Время') { |t| t.publish_date.strftime('%T') }

            column('Текст') { |t| link_to t.short_text, admin_group_post_path(t.group, t) }
          end
        end
      end

      column do
        panel 'Последние комментарии' do
          table_for Comment.order(id: :desc).limit(10) do

            column('Проверка') do |post|
              if post.check_result
                status_tag 'ok', :ok
              else
                status_tag 'fail', :red
              end
            end

            column('Группа') { |t| link_to t.group.name, admin_group_path(t.group) }

            column('Время') { |t| t.publish_date.strftime('%T') }

            column('Текст') do |t|
              if t.subject.kind_of? Post
                link_to t.short_text, admin_group_post_path(t.group, t.subject)
              elsif t.subject.kind_of? Topic
                link_to t.short_text, admin_group_topic_path(t.group, t.subject)
              else
                t.short_text
              end
            end
          end
        end
      end

    end

    columns do

      column do
        panel 'Последние удаленные посты посты' do
          table_for Post.where(check_result: false).order(id: :desc).limit(10) do

            column('Группа') { |t| link_to t.group.name, admin_group_path(t.group) }

            column('Время') { |t| t.publish_date.strftime('%T') }

            column('Текст') { |t| link_to t.short_text, admin_group_post_path(t.group, t) }
          end
        end
      end

      column do
        panel 'Последние удаленные комментарии' do
          table_for Comment.where(check_result: false).order(id: :desc).limit(10) do

            column('Группа') { |t| link_to t.group.name, admin_group_path(t.group) }

            column('Время') { |t| t.publish_date.strftime('%T') }

            column('Текст') do |t|
              if t.subject.kind_of? Post
                link_to t.short_text, admin_group_post_path(t.group, t.subject)
              elsif t.subject.kind_of? Topic
                link_to t.short_text, admin_group_topic_path(t.group, t.subject)
              else
                t.short_text
              end
            end
          end
        end
      end

    end

  end

end
