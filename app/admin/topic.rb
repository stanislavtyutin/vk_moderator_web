ActiveAdmin.register Topic do
  belongs_to :group
  # permit_params :email, :password, :password_confirmation

  actions :all, except: [:edit, :new, :destroy]

  index do
    selectable_column
    # id_column
    column :vk_id do |t|
      link_to t.vk_id, "https://vk.com/topic-#{t.group.vk_id}_#{t.vk_id}"
    end

    column :title

    actions
  end

  show do
    attributes_table do
      row :group
      row :vk_id do
        link_to topic.vk_id, "https://vk.com/topic-#{topic.group.vk_id}_#{topic.vk_id}"
      end
      row :comments do
        topic.comments.count
      end
    end

    panel 'Последнии комментарии' do
      table_for topic.comments.order(id: :desc).take(20) do
        column('ID Вконтакте') { |t| t.vk_id }
        column('Время публикации') { |t| t.publish_date.strftime('%Y-%m-%d %H:%M:%S') }
        column('Результат проверки') { |t| t.check_result ? status_tag('ok', :ok) : status_tag('fail', :red) }
        column('Текст') { |t| t.short_text }
      end
    end

    active_admin_comments
  end


  filter :group
  filter :publish_date
  #
  # filter :email
  # filter :current_sign_in_at
  # filter :sign_in_count
  # filter :created_at
  #
  # form do |f|
  #   f.inputs "Admin Details" do
  #     f.input :email
  #     f.input :password
  #     f.input :password_confirmation
  #   end
  #   f.actions
  # end

end
