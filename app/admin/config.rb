ActiveAdmin.register_page 'Config', as: 'Настройки' do
  menu label: 'Настройки', priority: 4
  content title: 'Настройки' do
    render partial: 'config'
  end

  page_action :save, method: :post do
    config = {
        stop_words: params[:stop_words].split(',').map { |t| t.strip },
        post_timeout: params[:post_timeout].to_i,
        old_timeout: params[:old_timeout].to_i,
        min_followers: params[:min_followers].to_i,
        min_friends: params[:min_friends].to_i,
        request_invite_count: params[:request_invite_count].to_i,
        max_timetable_offset: params[:max_timetable_offset].to_i
    }

    CoreApi.instance.set_config config

    redirect_to admin_config_path, notice: 'Настройки сохранены'
  end
end