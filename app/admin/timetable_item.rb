ActiveAdmin.register TimetableItem do
  belongs_to :group
  permit_params :group_id, :hour, :min, :reserved

  actions :all, except: [:show]

  index do
    selectable_column
    # id_column
    column :time do |t|
      "#{t.hour}:#{t.min}"
    end
    column :reserved
    actions
  end

  filter :reserved

  sidebar 'Генерация расписания', only: :index, if: proc { not group.timetable_items.any? }, partial: 'generate_sidebar'

  collection_action :generate, method: :post do
    group = Group.find params.require(:group_id)
    interval = params.require(:interval).to_i

    time = [0, 0]
    while time[0] < 24 do
      TimetableItem.create group: group, hour: time[0], min: time[1]
      time[1] += interval
      while time[1] >= 60 do
        time[1] -= 60
        time[0] += 1
      end
    end

    redirect_to admin_group_timetable_items_path(group)
  end

  form do |f|
    f.inputs do
      f.input :group, as: :hidden
      f.input :hour
      f.input :min
      f.input :reserved
    end
    f.actions
  end

end
