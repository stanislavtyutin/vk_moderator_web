ActiveAdmin.register Group do
  menu priority: 2, label: 'Группы'

  permit_params :name, :vk_code, :vk_secret, :access_token, :passthrough_url, :active,
                :stop_words,
                :enable_check_wall,
                :enable_check_topics,
                :enable_check_photos,
                :enable_check_videos,
                :enable_check_market_items,
                :enable_check_stop_words,
                :enable_check_links,
                :enable_check_text_exists,
                :enable_check_membership,
                :enable_check_timeout,
                :enable_remove_old_posts,
                :enable_remove_old_topic_comments,
                :enable_auto_publish_suggested,
                :enable_suggest_publish_from_group,
                :enable_auto_answering,
                :enable_message_on_reject,
                :post_timeout,
                :old_timeout,
                :post_request_messages_template,
                :post_reject_messages_template

  actions :all, except: [:new]

  index do
    selectable_column
    # id_column
    column :name
    column :vk_id do |t|
      link_to t.vk_id, "https://vk.com/club#{t.vk_id}"
    end
    column :active
    actions
  end


  filter :active
  filter :name
  filter :vk_id

  show do
    attributes_table do
      row :name
      row :vk_id
      row :active
      row :vk_code
      row :vk_secret
      row :access_token
      row :passthrogh_url
      row :stop_words
      row :city
    end

    panel 'Боты' do
      table_for group.bots do
        column :name do |t|
          link_to t.name, admin_bot_path(t)
        end
        column :vk_id
        column :active
      end
    end

    panel 'Последние посты' do
      table_for group.posts.order(id: :desc).take(10) do
        column :vk_id do |t|
          link_to t.vk_id, "https://vk.com/club#{group.vk_id}?w=wall-#{group.vk_id}_#{t.vk_id}/all"
        end
        column :publish_date
        column :check_result
        column :text do |t|
          link_to t.short_text, admin_group_post_path(group, t)
        end
      end

      a href: admin_group_posts_path(group) do
        button 'Перейти ко всем постам'
      end
    end

    panel 'Обсуждения' do
      if group.topics.any?
        table_for group.topics do
          column :vk_id do |t|
            link_to t.vk_id, "https://vk.com/topic-#{t.group.vk_id}_#{t.vk_id}"
          end
          column :title do |t|
            link_to t.title, admin_group_topic_path(group, t)
          end
        end
      end

      a href: load_topics_list_admin_group_path(group) do
        button 'Синхролизировать список обсуждений'
      end
    end

    panel 'Структура автоответчика' do
      if group.dialog_structure
        a href: admin_dialog_structure_path(group.dialog_structure) do
          button 'Перейти'
        end
      else
        a href: new_admin_dialog_structure_path(dialog_structure: {group_id: group.id}) do
          button 'Создать'
        end
      end
    end

    panel 'Настройки модерации' do
      attributes_table_for group do
        row :enable_check_wall
        row :enable_check_topics
        # row :enable_check_photos
        # row :enable_check_videos
        # row :enable_check_market_items
        row :enable_check_stop_words
        row :enable_check_links
        row :enable_check_text_exists
        row :enable_check_membership
        row :enable_check_timeout
        row :enable_remove_old_posts
        row :enable_remove_old_topic_comments
        row :enable_auto_publish_suggested
        row :enable_suggest_publish_from_group
        row :enable_auto_answering
        row :enable_message_on_reject
        row :post_timeout
        row :old_timeout
      end
    end

    panel 'Белый список пользователей (активные)' do
      scope = group.white_users.where('stop_date > ?', Time.now)
      table_for scope do
        column :vk_id
        column :name
        column :start_date
        column :stop_date
      end if scope.any?

      a href: admin_group_white_users_path(group) do
        button 'Перейти по всем'
      end
    end

    panel 'Белый список ссылок (активные)' do
      scope = group.white_links.where('stop_date > ?', Time.now)
      table_for scope do
        column :link
        column :start_date
        column :stop_date
      end if scope.any?

      a href: admin_group_white_links_path(group) do
        button 'Перейти по всем'
      end
    end

    panel 'Расписание группы (зарезервированные)' do
      scope = group.timetable_items.where(reserved: true)
      table_for scope do
        column :time do |t|
          "#{t.hour}:#{t.min}"
        end
        # column :reserved
      end if scope.any?

      a href: admin_group_timetable_items_path(group) do
        button 'Перейти по всем'
      end
    end

    active_admin_comments
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :vk_code
      f.input :vk_secret
      f.input :access_token
      f.input :passthrough_url
      f.input :city
      f.input :active
    end

    panel 'Установка приложения' do
      a href: CoreApi.instance.app_group_install_link(group.vk_id), 'target': 'blank' do
        'Установить приложение'
      end
    end

    f.inputs 'Настройки модерации' do
      f.input :stop_words, as: :text
      f.input :enable_check_wall
      f.input :enable_check_topics
      # f.input :enable_check_photos
      # f.input :enable_check_videos
      # f.input :enable_check_market_items
      f.input :enable_check_stop_words
      f.input :enable_check_links
      f.input :enable_check_text_exists
      f.input :enable_check_membership
      f.input :enable_check_timeout
      f.input :enable_remove_old_posts
      f.input :enable_remove_old_topic_comments
      f.input :enable_auto_publish_suggested
      f.input :enable_suggest_publish_from_group
      f.input :enable_auto_answering
      f.input :enable_message_on_reject
      f.input :post_timeout
      f.input :old_timeout
    end


    f.inputs 'Шаблоны сообщений' do
      f.inputs :post_request_messages_template, as: :text if group.enable_auto_publish_suggested
      f.inputs :post_reject_messages_template, as: :text if group.enable_message_on_reject
    end if group.enable_message_on_reject || group.enable_auto_publish_suggested

    f.actions
  end

  member_action :load_topics_list, method: :get do
    resource.sync
    redirect_to resource_path(resource), notice: 'Обсуждения синхролизированы'
  end

end
