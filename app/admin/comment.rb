ActiveAdmin.register Comment, as: 'VkComment' do
  belongs_to :post #, polymorphic: true
  # belongs_to :topic, polymorphic: true
  # permit_params :email, :password, :password_confirmation

  actions :all, except: [:edit, :new, :destroy]

  index do
    selectable_column
    # id_column
    column :vk_id
    column :check_result
    column :is_deleted
    column :publish_date
    column :text do |t|
      t.short_text
    end
    actions
  end

  show do
    attributes_table do
      row :group
      row :vk_id
      row :from_vk_id
      row :publish_date
      row :check_result
      row :is_deleted
      row :text
    end

    unless vk_comment.check_result
      panel 'Причины удаления' do

      end
    end

    active_admin_comments
  end
  #
  # filter :email
  # filter :current_sign_in_at
  # filter :sign_in_count
  # filter :created_at
  #
  # form do |f|
  #   f.inputs "Admin Details" do
  #     f.input :email
  #     f.input :password
  #     f.input :password_confirmation
  #   end
  #   f.actions
  # end

end
