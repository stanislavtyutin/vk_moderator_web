ActiveAdmin.register DialogStructure do
  permit_params :text, :key, :back_key, :reset_key, :parent_id, :group_id

  #actions :all, except: [:index]
  menu false
  breadcrumb { false }

  controller do
    def create
      create! do |format|
        format.html { redirect_to admin_dialog_structure_path(resource.base_element) }
      end
    end

    def update
      update! do |format|
        base_element = resource
        until base_element.parent.nil?
          base_element = base_element.parent
        end
        format.html { redirect_to admin_dialog_structure_path(resource.base_element) }
      end
    end

    def destroy
      destroy! do |format|
        base_element = resource.base_element
        if base_element == resource
          format.html { redirect_to admin_group_path(base_element.group) }
        else
          format.html { redirect_to admin_dialog_structure_path(base_element) }
        end
      end
    end

    def show
      if resource == resource.base_element
        super
      else
        redirect_to admin_dialog_structure_path(resource.base_element)
      end
    end

    def index
      redirect_to admin_root_path
    end
  end

  # noinspection RubyArgCount
  show title: 'Сообщения автоответчика' do

    def self.print_dialog_structure(ds)
      panel 'Сообщение' do
        columns do
          column do
            span "Текст: #{ds.text}"
            br
            span "Ключевое слово: #{ds.key}"
            br
            span "Переход назад: #{ds.back_key}"
            br
            span "Переход в начало: #{ds.reset_key}"
          end

          column do
            a(href: edit_admin_dialog_structure_path(ds)) { 'Изменить' }
            br
            if ds.parent
              a(href: admin_dialog_structure_path(ds),
                'data-confirm' => 'Вы уверены?',
                'data-method' => 'delete') { 'Удалить' }
              br
            end
            a(href: new_admin_dialog_structure_path(dialog_structure: {parent_id: ds.id})) { 'Добавить' }
          end
        end

        ds.children.all.each { |t| print_dialog_structure t }
      end
    end

    panel 'Структура автоответчика' do
      print_dialog_structure dialog_structure
    end

    panel 'Переход' do
      a href: admin_group_path(dialog_structure.group) do
        button 'Назад к группе'
      end
    end

  end


  form do |f|
    f.inputs do
      f.input :group_id, as: :hidden
      f.input :parent_id, as: :hidden
      f.input :text
      f.input :key
      f.input :back_key
      f.input :reset_key
    end
    f.actions do
      f.action(:submit)
      begin
        f.cancel_link(admin_dialog_structure_path(resource.base_element))
      rescue
        nil
      end
    end
  end


end