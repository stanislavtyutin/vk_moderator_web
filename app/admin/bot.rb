ActiveAdmin.register Bot do
  menu priority: 3, label: 'Боты'

  permit_params :name, :vk_id, :access_token, :active

  index do
    selectable_column
    # id_column
    column :name
    column :vk_id do |t|
      link_to t.vk_id, "https://vk.com/id#{t.vk_id}"
    end
    column :active
    actions
  end

  filter :groups
  filter :active
  filter :name

  show do
    attributes_table do
      row :name
      row :vk_id
      row :active
      row :access_token
    end

    panel 'Группы' do
      table_for bot.groups do
        column :name do |t|
          link_to t.name || 'Название не известно', admin_group_path(t)
        end
        column :vk_id do |t|
          link_to t.vk_id, "https://vk.com/club#{t.vk_id}"
        end
        column :active
      end

      a href: load_groups_list_admin_bot_path(bot) do
        button 'Синхролизировать список групп'
      end
    end

    active_admin_comments
  end

  form do |f|
    f.inputs do
      f.input :vk_id
      f.input :access_token
      f.input :name
      f.input :active
    end

    panel 'Установка приложения' do
      a href: CoreApi.instance.app_install_link, 'target': 'blank' do
        'Установить приложение'
      end
    end

    f.actions
  end

  member_action :load_groups_list, method: :get do
    resource.load_groups
    redirect_to resource_path(resource), notice: 'Группы синхролизированы'
  end

  #
  # filter :email
  # filter :current_sign_in_at
  # filter :sign_in_count
  # filter :created_at
  #
  # form do |f|
  #   f.inputs "Admin Details" do
  #     f.input :email
  #     f.input :password
  #     f.input :password_confirmation
  #   end
  #   f.actions
  # end

end
