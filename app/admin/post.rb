ActiveAdmin.register Post do
  belongs_to :group
  permit_params :regular_removing

  actions :all, except: [:new, :destroy]

  index do
    selectable_column
    # id_column
    column :vk_id do |t|
      link_to t.vk_id, "https://vk.com/club#{t.group.vk_id}?w=wall-#{t.group.vk_id}_#{t.vk_id}/all"
    end
    column :from_vk_id do |t|
      link_to t.from_vk_id, "https://vk.com/id#{t.from_vk_id}"
    end
    column :check_result
    column :is_deleted
    column :post_type if group.posts.pluck(:post_type).uniq.size > 1
    column :publish_date
    column :text do |t|
      t.short_text
    end
    actions
  end

  show do
    attributes_table do
      row :group
      row :vk_id do
        link_to post.vk_id, "https://vk.com/club#{post.group.vk_id}?w=wall-#{post.group.vk_id}_#{post.vk_id}/all"
      end
      row :from_vk_id
      row :post_type
      row :publish_date
      row :check_result
      row :is_deleted
      row :regular_removing
      row :text
      row :comments do
        post.comments.count
      end
    end

    unless post.check_result
      panel 'Причины удаления' do
        table_for post.get_removing_reason.map { |k, v| [k, v] } do
          column('Условие') { |t| t[0].to_s }
          column('Результат') do |t|
            case t[1]
              when TrueClass
                status_tag 'ok', :ok
              when FalseClass
                status_tag 'fail', :red
              else
                t[1]
            end
          end
        end
      end
    end

    if post.comments.any?
      panel 'Последнии комментарии' do
        table_for post.comments.order(id: :desc).take(20) do
          column('ID Вконтакте') { |t| t.vk_id }
          column('Время публикации') { |t| t.publish_date.strftime('%Y-%m-%d %H:%M:%S') }
          column('Результат проверки') { |t| t.check_result ? status_tag('ok', :ok) : status_tag('fail', :red) }
          column('Текст') { |t| t.short_text }
        end

        a href: admin_post_vk_comments_path(post) do
          button 'Перейти ко всем комментариям'
        end
      end
    end

    active_admin_comments
  end

  #filter :group
  filter :from_vk_id
  filter :publish_date
  filter :post_type, as: :select,
         collection: [['Обычная запись', 'post'],
                      ['Предложенная запись', 'suggest'],
                      ['Репост', 'copy'],
                      ['Отложенная запись', 'postpone'],
                      ['Ответ', 'reply']]
  filter :text
  filter :check_result
  filter :is_deleted
  filter :regular_removing

  form do |f|
    f.inputs do
      f.input :regular_removing
    end
    f.actions
  end

end
