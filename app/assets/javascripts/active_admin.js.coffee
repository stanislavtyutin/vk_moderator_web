#= require active_admin/base

#= require chosen-jquery

#= require active_admin/base

$ ->
# enable chosen js
  $('.chosen-select').chosen
    allow_single_deselect: true
    no_results_text: 'No results matched'
    width: '300px'