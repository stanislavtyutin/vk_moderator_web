# config valid only for current version of Capistrano
lock '3.4.1'

set :application, 'vk_moderator_web'
set :repo_url, 'ssh://gitlab@git.tbrain.ru:2222/stanislav.tyutin/vk_moderator_web.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/opt/deploy/vk_moderator_web'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml', 'unicorn.rb', 'config/application.rb')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
set :default_env, { 'RACK_ENV' => 'production' }

# Default value for keep_releases is 5
# set :keep_releases, 5

# set :rbenv_type, :user # or :system, depends on your rbenv setup
# set :rbenv_ruby, '2.3.1'

# after 'deploy:publishing', 'deploy:restart'

set :pid, -> { File.join(current_path, 'tmp', 'pids', 'unicorn.pid') }

after 'deploy', 'deploy:restart'
