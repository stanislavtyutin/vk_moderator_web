class CoreApi
  require 'net/http'
  require 'socket'
  include Singleton

  HTTP_VERSION = '1.1'.freeze
  HTTP_OK = '200'.freeze

  def initialize
    @socket_path = Rails.configuration.core_api[:socket_path]
  end

  def bot_sync(id)
    response = make_request "/api/bot/#{id}/update"
    response.code == HTTP_OK
  rescue
    false
  end

  def bot_load_groups(id)
    response = make_request "/api/bot/#{id}/load_groups"
    response.code == HTTP_OK
  rescue
    false
  end

  def group_sync(id)
    response = make_request "/api/group/#{id}/update"
    response.code == HTTP_OK
  rescue
    false
  end

  def white_user_sync(id)
    response = make_request "/api/white_user/#{id}/update"
    response.code == HTTP_OK
  rescue
    false
  end

  def get_removing_reason(type, id)
    response = make_request "/api/#{type}/#{id}/removing_reason"
    if response.code == HTTP_OK
      data = JSON.parse response.body
      data.symbolize_keys
    else
      {}
    end
  rescue
    {}
  end

  def get_config
    response = make_request '/api/config'
    data = JSON.parse response.body
    data.symbolize_keys
  end

  def set_config(config)
    response = make_request '/api/config', :post, config.to_json
    response.code == HTTP_OK
  rescue
    false
  end

  def app_install_link
    response = make_request '/api/app_install_link'
    response.body
  end

  def app_group_install_link(vk_id)
    response = make_request "/api/app_group_install_link?vk_id=#{vk_id}"
    response.body
  end

  def make_request(request_string, method=:get, body=nil)
    sock = Net::BufferedIO.new(UNIXSocket.new(@socket_path))

    if method == :get
      request = Net::HTTP::Get.new(request_string)
    elsif method == :post
      request = Net::HTTP::Post.new(request_string)
      request.body = body
    else
      return nil
    end

    request.exec(sock, HTTP_VERSION, request_string)

    begin
      response = Net::HTTPResponse.read_new(sock)
    end while response.kind_of?(Net::HTTPContinue)
    response.reading_body(sock, request.response_body_permitted?) {}

    response
  end

end